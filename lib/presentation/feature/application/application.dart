import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:name_of_project/presentation/core/router/memory_router.dart';
import 'package:name_of_project/presentation/util/theme/memory_ligth_theme.dart';

class MyApp extends ConsumerWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return MaterialApp.router(
      title: 'Flutter Demo',
      theme: MemoryLightTheme().themeData(),
      onGenerateTitle: (context) => 'bip',
      routerConfig: smaRouter,
    );
  }
}
