import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:name_of_project/data/data.dart';
import 'package:name_of_project/presentation/feature/application/application.dart';

Future<void> main() async {
  await runZonedGuarded(
    () async {
      WidgetsFlutterBinding.ensureInitialized();

      await dotenv.load(fileName: '.config/.env');

      await Data.init();

      runApp(const ProviderScope(child: MyApp()));
    },
    (error, stack) {
      debugPrint(error.toString());
    },
  );
}
