import 'package:flutter/material.dart';
import 'package:name_of_project/presentation/widget/functional/scaffold/memory_background.dart';

class MemoryScaffold extends StatelessWidget {
  const MemoryScaffold({
    super.key,
    required this.title,
    required this.child,
  });

  final String title;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(title),
      ),
      body: MemoryBackground(child: child),
    );
  }
}
