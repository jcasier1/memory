import 'package:freezed_annotation/freezed_annotation.dart';

part 'sign_model.freezed.dart';

@freezed
class SignModel with _$SignModel {
  SignModel._();

  factory SignModel({
    @Default('') String login,
    @Default('') String password,
  }) = _Data;
}
