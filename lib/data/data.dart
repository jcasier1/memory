import 'package:name_of_project/data/src/http/http.dart';

class Data {
  static Future<void> init() async {
    // Init http layer
    Http.init();
  }
}
