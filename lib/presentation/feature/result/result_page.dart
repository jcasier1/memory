import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:name_of_project/presentation/notifier/loading_notifier.dart';
import 'package:name_of_project/presentation/widget/functional/card/memory_card.dart';
import 'package:name_of_project/presentation/widget/functional/scaffold/memory_scaffold.dart';

class ResultPage extends HookConsumerWidget {
  const ResultPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final loading = ref.watch(loadingProvider);

    return MemoryScaffold(
      title: 'bip',
      child: Column(
        children: <Widget>[
          Row(
            children: [
              Expanded(
                child: MemoryCard(
                  title: 'bip',
                  loading: loading,
                  onTap: () => onTapAction1(ref),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: MemoryCard(
                  title: 'bip' 'bip',
                  loading: loading,
                  onTap: () => _onTapAction2(ref),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<void> onTapAction1(WidgetRef ref) async {
    try {
      print('Welcome');
    } finally {
      print('end');
    }
  }

  Future<void> _onTapAction2(WidgetRef ref) async {
    try {
      print('Welcome');
    } finally {
      print('end');
    }
  }
}
