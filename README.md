# Flutter template

## Contents

1. [General information](#General-information)
2. [Installation](#installation)

## General information

This App is a boilerplate using Next13 JS (with App directory), Typescript, SCSS, axios ans basic styles.

## Installation

### To start a project

- Clone this repository
- go inside. In command prompt run :
  **'flutter pub get'**
  **'dart run build_runner build'**
- If intl is used (in case of an international website) run
  **'flutter gen-l10n'**

- Configure the .env file in .env if there is a back office

- Open an emulator or connect a device and run the app (from your code editor or in your command prompt write 'run flutter' app)

### List of commands and their meaning

To complete with vscode
