import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class MemoryBackground extends HookWidget {
  const MemoryBackground({
    super.key,
    required this.child,
  });

  final Widget child;

  @override
  Widget build(BuildContext context) {
    final controller = useScrollController();

    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        SingleChildScrollView(
          controller: controller,
          padding: const EdgeInsets.only(top: 20, right: 20, bottom: 0, left: 20),
          child: child,
        ),
      ],
    );
  }
}
