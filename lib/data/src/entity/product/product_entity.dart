import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:name_of_project/data/src/converter/string_to_double_converter.dart';

part 'product_entity.freezed.dart';
part 'product_entity.g.dart';

@freezed
abstract class ProductEntity with _$ProductEntity {
  ProductEntity._();

  @JsonSerializable(explicitToJson: true, includeIfNull: false)
  factory ProductEntity.dto({
    @JsonKey(name: 'SKU9') @Default('') String sku,
    @JsonKey(name: 'Tarif') @Default(-1) @StringToDouble() double price,
    @JsonKey(name: 'TarifRemise') @Default(-1) @StringToDouble() double discountPrice,
    @JsonKey(name: 'tauxDeRemise') @Default(1) @StringToDouble() double discountRate,
  }) = ProductDto;

  factory ProductEntity.fromJson(Map<String, Object?> json) => _$ProductEntityFromJson(json);
}
