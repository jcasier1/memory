import 'package:freezed_annotation/freezed_annotation.dart';

class StringToDouble extends JsonConverter<double, String> {
  const StringToDouble();

  @override
  double fromJson(String json) {
    return json.isEmpty ? 0 : (double.tryParse(json) ?? -1);
  }

  @override
  String toJson(double object) {
    return object.toString();
  }
}
