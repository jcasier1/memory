import 'package:go_router/go_router.dart';
import 'package:name_of_project/presentation/core/router/memory_route.dart';
import 'package:name_of_project/presentation/feature/play/play_page.dart';
import 'package:name_of_project/presentation/feature/result/result_page.dart';
import 'package:name_of_project/presentation/feature/welcome/welcome_page.dart';

final smaRouter = GoRouter(
  routes: <RouteBase>[
    GoRoute(
      path: '/',
      name: kWelcomePage,
      builder: (context, state) {
        return const WelcomePage();
      },
    ),
    GoRoute(
      path: '/play',
      name: kPlayPage,
      builder: (context, state) => const PlayPage(),
    ),
    GoRoute(
      path: '/result',
      name: kResultPage,
      builder: (context, state) => const ResultPage(),
    ),
  ],
);
