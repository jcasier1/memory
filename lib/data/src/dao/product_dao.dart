import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:name_of_project/data/src/entity/product/product_entity.dart';
import 'package:name_of_project/data/src/http/http.dart';

final productDaoHttpProvider = Provider((ref) => _ProductDao(ref));

class _ProductDao {
  _ProductDao(this.ref);

  final Ref ref;
  final Dio dio = Http.instance.dio;

  Future<List<ProductDto>> fetchAll() async {
    const url = 'shop/products';

    try {
      final resp = await dio.get<List<dynamic>>(url);
      return (resp.data ?? []).map((e) => ProductDto.fromJson(e)).toList();
    } on Exception catch (e) {
      debugPrint(e.toString());
      return [];
    }
  }
}
