import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:name_of_project/presentation/notifier/loading_notifier.dart';
import 'package:name_of_project/presentation/widget/functional/card/memory_card.dart';
import 'package:name_of_project/presentation/widget/functional/scaffold/memory_scaffold.dart';

class WelcomePage extends HookConsumerWidget {
  const WelcomePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final loading = ref.watch(loadingProvider);

    return MemoryScaffold(
      title: 'Bonjour',
      child: Column(
        children: <Widget>[
          Row(
            children: [
              Expanded(
                child: MemoryCard(
                  title: 'CArte 1',
                  loading: loading,
                  onTap: () => onTapAction1(ref),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: MemoryCard(
                  title: 'bip' 'bip',
                  loading: loading,
                  onTap: () => _onTapAction2(ref),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<void> onTapAction1(WidgetRef ref) async {
    try {
      print('Welcome');
    } finally {
      print('end');
    }
  }

  Future<void> _onTapAction2(WidgetRef ref) async {
    try {
      print('Welcome');
    } finally {
      print('end');
    }
  }
}
